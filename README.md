
# **Lab 1 : Configuration et utilisation de scanners de sécurité OSS**

## **Mise en situation : Une mission critique pour les agents de la cybersécurité**
Bienvenue dans votre première mission en tant qu’agents de sécurité pour l’agence fictive **CyberShield**. Une entreprise cliente, **DataFlex Inc.**, vient de lancer un nouveau projet web critique, mais les développeurs sont préoccupés par d'éventuelles vulnérabilités dans les dépendances utilisées. Votre mission : configurer un scanner de sécurité open-source, analyser les dépendances et proposer des recommandations basées sur les résultats.

Votre objectif : identifier et corriger les vulnérabilités dans leurs dépendances Node.js avant le déploiement.

---

## **Objectif**
Apprendre à configurer et utiliser **Dependency-Check**, un outil open-source d'analyse des dépendances, pour identifier des vulnérabilités connues.

---

## **Prérequis**

1. **Connectez-vous en tant que superutilisateur :**
   Passez en mode superutilisateur pour exécuter les commandes nécessitant des privilèges administratifs :
   ```bash
   sudo su
   ```
   **Explication** : Certaines commandes nécessitent des droits élevés pour installer des outils ou configurer le système.

2. **Installez les outils de base :**
   Assurez-vous que les outils suivants sont installés :
   ```bash
   apt-get update
   apt-get install -y unzip wget npm default-jre
   ```
   **Explication** : Ces outils sont nécessaires pour télécharger, installer et exécuter Dependency-Check ainsi que le projet Node.js.

---

## **Étapes pour réaliser le lab**

### **Étape 1 : Création d’un projet à analyser**
Nous allons créer un projet Node.js avec des dépendances intentionnellement vulnérables.

1. **Créez le répertoire du projet :**
   ```bash
   mkdir my_vulnerable_project
   cd my_vulnerable_project
   ```
   **Explication** : Ce répertoire servira de base pour notre projet fictif contenant des dépendances vulnérables.

2. **Ajoutez le fichier `package.json` :**
   ```bash
   cat << 'EOF' > package.json
   {
     "name": "my_vulnerable_project",
     "version": "1.0.0",
     "description": "A Node.js project with vulnerable dependencies",
     "main": "index.js",
     "dependencies": {
       "express": "4.16.1",
       "lodash": "4.17.15",
       "moment": "2.19.3",
       "jquery": "3.4.1"
     },
     "scripts": {
       "start": "node index.js"
     },
     "author": "",
     "license": "ISC"
   }
   EOF
   ```
   **Explication** : Ce fichier définit les dépendances utilisées par le projet. Les versions choisies contiennent des vulnérabilités connues.

3. **Créez le fichier principal `index.js` :**
   ```bash
   cat << 'EOF' > index.js
   const express = require('express');
   const moment = require('moment');
   const app = express();

   app.get('/', (req, res) => {
     res.send('Hello, world! ' + moment().format());
   });

   app.listen(3000, () => {
     console.log('Server is running on port 3000');
   });
   EOF
   ```
   **Explication** : Ce fichier est le point d’entrée de l’application et utilise les dépendances définies dans `package.json`.

4. **Installez les dépendances :**
   ```bash
   npm install
   ```
   **Explication** : Cette commande télécharge et installe les bibliothèques nécessaires pour exécuter l'application.

---

### **Étape 2 : Installation de Dependency-Check**
Dependency-Check est un outil open-source qui analyse les dépendances d’un projet pour identifier des vulnérabilités connues.

1. **Téléchargez et installez Dependency-Check :**
   ```bash
   wget https://github.com/jeremylong/DependencyCheck/releases/download/v6.5.3/dependency-check-6.5.3-release.zip -O dependency-check.zip
   sudo unzip dependency-check.zip -d /opt/
   export PATH=$PATH:/opt/dependency-check/bin
   ```
   **Explication** : 
   - `wget` télécharge l'outil depuis GitHub.
   - `unzip` extrait les fichiers dans `/opt/` pour une installation globale.
   - `export PATH` permet d’exécuter l’outil depuis n’importe où.

---

### **Étape 3 : Configuration et exécution du scanner**
Nous allons utiliser Dependency-Check pour analyser notre projet.

1. **Définissez les variables :**
   ```bash
   PROJECT_PATH="$PWD"
   OUTPUT_DIR="$PWD/report"
   mkdir -p "$OUTPUT_DIR"
   ```

Ou définissez directement les variables à la main :

   ```bash
   export PROJECT_PATH="/home/NOM PRENOM/my_vulnerable_project"
   export OUTPUT_DIR="/home/simon_renaud/my_vulnerable_project"/report"
   mkdir report
   ```

   **Explication** : 
   - `PROJECT_PATH` désigne le chemin du projet à scanner.
   - `OUTPUT_DIR` est le dossier où seront enregistrés les rapports.

2. **Exécutez le scan :**
   ```bash
   dependency-check.sh --project "MyVulnerableProject" --scan "$PROJECT_PATH" --format "ALL" --out "$OUTPUT_DIR"
   ```
   **Explication** : 
   - `--project` nomme le projet analysé.
   - `--scan` indique le chemin des fichiers à analyser.
   - `--format "ALL"` génère des rapports dans plusieurs formats (HTML, CSV, XML, JSON).
   - `--out` spécifie le répertoire de sortie.

3. **Ouvrez le rapport :**
   ```bash
   if command -v xdg-open &> /dev/null
   then
       xdg-open "$OUTPUT_DIR/dependency-check-report.html"
   else
       echo "Report available at $OUTPUT_DIR/dependency-check-report.html"
   fi
   ```
   **Explication** : Cette commande tente d’ouvrir le rapport HTML dans un navigateur, ou indique son emplacement si aucun navigateur n’est disponible.

---

### **Étape 4 : Analyse et remédiation**
1. **Interprétez le rapport :**
   - Ouvrez le fichier HTML et examinez les vulnérabilités listées.
   - Notez leur gravité (critique, élevée, moyenne, faible) et les recommandations fournies.

2. **Corrigez les vulnérabilités :**
   Mettez à jour les dépendances obsolètes avec :
   ```bash
   npm install <package>@latest
   ```
   **Exemple** : Pour mettre à jour `lodash` :
   ```bash
   npm install lodash@latest
   ```

---

### **Étape 5 : Intégration dans CI/CD (Pas a faire)**
Voici un exemple de configuration pour intégrer Dependency-Check dans un pipeline GitLab CI/CD :

```yaml
dependency-check:
  image: openjdk:11
  script:
    - wget https://github.com/jeremylong/DependencyCheck/releases/download/v6.5.3/dependency-check-6.5.3-release.zip -O dependency-check.zip
    - unzip dependency-check.zip -d /opt/
    - /opt/dependency-check/bin/dependency-check.sh --project "CI_Project" --scan "$CI_PROJECT_DIR" --format "ALL" --out "$CI_PROJECT_DIR/reports"
  artifacts:
    paths:
      - reports
    when: always
```
**Explication** : 
- Ce pipeline exécute Dependency-Check sur le code à chaque commit et enregistre les rapports.

---

## **Conclusion**
Félicitations ! Vous avez analysé un projet Node.js, identifié des vulnérabilités et proposé des corrections. Cette méthodologie peut être appliquée à tout projet utilisant des dépendances externes.

**Défi bonus :** Explorez d’autres outils comme **Snyk** ou **Trivy** pour diversifier vos analyses de sécurité. 🎯
